package cc.iotkit.data.mapper;

import cc.iotkit.data.model.TbChannelConfig;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ChannelConfigMapper extends BaseMapper<TbChannelConfig> {
}

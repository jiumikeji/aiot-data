package cc.iotkit.data.service;

import cc.iotkit.data.model.TbCategory;
import com.baomidou.mybatisplus.extension.service.IService;

public interface CategoryService extends IService<TbCategory> {
}

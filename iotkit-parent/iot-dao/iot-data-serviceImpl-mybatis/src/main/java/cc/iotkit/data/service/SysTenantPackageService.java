package cc.iotkit.data.service;

import cc.iotkit.data.model.TbSysTenantPackage;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SysTenantPackageService extends IService<TbSysTenantPackage> {
}
